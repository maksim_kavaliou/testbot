﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T_ShirtsTestBot.State
{
    public class Tshirts : ITshirts
    {
        private readonly List<string> _tshirts;

        private int _current = 0;

        public Tshirts()
        {
            _tshirts = new List<string>()
            {
                "https://kkaravaev.com/wa-data/public/shop/products/19/62/6219/images/9986/9986.970.jpg",
                "https://www.castlerock.ru/upload/iblock/ac2/ac20063a546ab27d005c70c5c89ddc48.jpg",
                "https://lamcdn.net/wonderzine.com/post_image-image/HC-pdkYVuuqydBQdhGw4oQ-wide.jpg",
                "https://i2.rozetka.ua/goods/7358570/54136524_images_7358570532.png"
            };
        }

        public string Next()
        {
            if (_current < _tshirts.Count - 1)
            {
                _current++;
            }
            else
            {
                _current = 0;
            }

            return Current;
        }
        public string Previous()
        {
            if (_current == 0)
            {
                _current = _tshirts.Count-1;
            }
            else
            {
                _current--;
            }

            return Current;
        }

        public string Current => _tshirts[_current];
    }
}
