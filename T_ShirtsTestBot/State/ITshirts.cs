﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T_ShirtsTestBot.State
{
    public interface ITshirts
    {
        string Next();
        string Previous();

        string Current { get; }
    }
}
