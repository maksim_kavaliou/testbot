﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;
using T_ShirtsTestBot.State;

namespace T_ShirtsTestBot.Commands
{
    public class TshirtsListCommand : Command<TshirtsListCommandModel>
    {
        private readonly ITshirts _tshirtsState;

        public TshirtsListCommand(IBotService botService, ITshirts tshirtsState) : base(botService)
        {
            _tshirtsState = tshirtsState;
        }

        public override async Task Execute(TshirtsListCommandModel commandModel)
        {
            var photoMessage = await _botService.Client.SendPhotoAsync(commandModel.ChatId, new InputOnlineFile(_tshirtsState.Current));

            var messageText = "Choose";

            var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
                new []
                {
                    InlineKeyboardButton.WithCallbackData("<", $"/prev_tshirt {photoMessage.MessageId}"),
                    InlineKeyboardButton.WithCallbackData(">", $"/next_tshirt {photoMessage.MessageId}")
                },
                new []
                {
                    InlineKeyboardButton.WithCallbackData("Buy", $"/buy {photoMessage.MessageId}")
                }
            });

            await _botService.Client.SendTextMessageAsync(commandModel.ChatId, messageText, replyMarkup: inlineKeyboard);
        }
    }
}
