﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;
using T_ShirtsTestBot.State;

namespace T_ShirtsTestBot.Commands
{
    public class NextTshirtCommand : Command<NextTshirtCommandModel>
    {
        private readonly ITshirts _tshirtsState;

        public NextTshirtCommand(IBotService botService, ITshirts tshirtsState) : base(botService)
        {
            _tshirtsState = tshirtsState;
        }

        public override async Task Execute(NextTshirtCommandModel commandModel)
        {
            await _botService.Client.AnswerCallbackQueryAsync(
                commandModel.BotUpdate.CallbackQuery.Id,
                $"Received {commandModel.BotUpdate.CallbackQuery.Data}");

            var messageId = Int32.Parse(commandModel.BotUpdate.CallbackQuery.Data.Split(' ').Last());
            var newMedia = new InputMediaPhoto(new InputMedia(_tshirtsState.Next()));

            await _botService.Client.EditMessageMediaAsync(commandModel.BotUpdate.CallbackQuery.Message.Chat.Id, messageId, newMedia);
        }
    }
}
