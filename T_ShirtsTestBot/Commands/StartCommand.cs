﻿using System;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Commands
{
    public class StartCommand : Command<StartCommandModel>
    {
        public StartCommand(IBotService botService) : base(botService)
        {
        }

        public override async Task Execute(StartCommandModel commandModel)
        {
            var user = commandModel.BotUpdate.Message.From;

            var fullName = user.FirstName;

            if (user.LastName != null)
            {
                fullName = $"{user.FirstName} {user.LastName}";
            }

            var messageText = $@"
                                { fullName}, добро пожалавать в магазин футболок! 👋 
                                Чтобы узнать больше о нашем магазине или сделать заказ футболки нажми на кнопки меню 👇";

            ReplyKeyboardMarkup replyKeyboard = new[]
            {
                new[] { "О компании", "Футболки" },
                new[] { "Связаться с оператором" },
            };

            await _botService.Client.SendTextMessageAsync(commandModel.ChatId, messageText, replyMarkup:replyKeyboard);
        }
    }
}
