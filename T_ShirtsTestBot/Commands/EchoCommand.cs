﻿using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Commands
{
    public class EchoCommand : Command<EchoCommandModel>
    {
        public EchoCommand(IBotService botService) : base(botService)
        {
        }

        public override async Task Execute(EchoCommandModel commandModel)
        {
            var update = commandModel.BotUpdate;

            if (update.Type != UpdateType.Message)
            {
                return;
            }

            var message = update.Message;


            if (message.Type == MessageType.Text)
            {
                await _botService.Client.SendTextMessageAsync(commandModel.ChatId, message.Text);
            }
        }
    }
}
