﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Commands
{
    public class BuyCommand : Command<BuyCommandModel>
    {
        public BuyCommand(IBotService botService) : base(botService)
        {
        }

        public override async Task Execute(BuyCommandModel commandModel)
        {
            await _botService.Client.AnswerCallbackQueryAsync(
                commandModel.BotUpdate.CallbackQuery.Id,
                $"Received {commandModel.BotUpdate.CallbackQuery.Data}");

            await _botService.Client.SendTextMessageAsync(commandModel.BotUpdate.CallbackQuery.Message.Chat.Id, "Отличный выбор :)");
        }
    }
}
