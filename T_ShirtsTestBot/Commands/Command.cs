﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Commands
{
    public abstract class Command<T> where T: CommandModel
    {
        protected readonly IBotService _botService;

        protected Command(IBotService botService)
        {
            _botService = botService;
        }

        public abstract Task Execute(T commandModel);
    }
}
