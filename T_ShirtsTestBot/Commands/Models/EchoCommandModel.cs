﻿using Telegram.Bot.Types;

namespace T_ShirtsTestBot.Commands.Models
{
    public class EchoCommandModel : CommandModel
    {
        public EchoCommandModel(Update botUpdate) : base(botUpdate)
        {
        }

        public override CommandType CommandType => CommandType.Echo;
    }
}
