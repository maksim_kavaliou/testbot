﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T_ShirtsTestBot.Commands.Models
{
    public enum CommandType
    {
        Start = 0,
        About = 1,
        Echo = 2,
        TshirtsList = 3,
        Buy = 4,
        NextTshirt = 5,
        PreviousTshirt = 6
    }
}
