﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace T_ShirtsTestBot.Commands.Models
{
    public class TshirtsListCommandModel : CommandModel
    {
        public TshirtsListCommandModel(Update botUpdate) : base(botUpdate)
        {
        }

        public override CommandType CommandType => CommandType.TshirtsList;
    }
}
