﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace T_ShirtsTestBot.Commands.Models
{
    public abstract class CommandModel
    {
        protected CommandModel(Update botUpdate)
        {
            BotUpdate = botUpdate;
        }

        public Update BotUpdate { get; set; }

        public long ChatId => BotUpdate.Message.Chat.Id;

        public abstract CommandType CommandType { get; }
    }
}
