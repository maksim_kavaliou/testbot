﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace T_ShirtsTestBot.Commands.Models
{
    public class AboutCommandModel : CommandModel
    {
        public AboutCommandModel(Update botUpdate) : base(botUpdate)
        {
        }

        public override CommandType CommandType => CommandType.About;
    }
}
