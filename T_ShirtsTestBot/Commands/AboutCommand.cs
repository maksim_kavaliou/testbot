﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T_ShirtsTestBot.Commands.Models;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Commands
{
    public class AboutCommand : Command<AboutCommandModel>
    {
        public AboutCommand(IBotService botService) : base(botService)
        {
        }

        public override async Task Execute(AboutCommandModel commandModel)
        {
            var messageText = $@"
            Наша компания продаёт лучшие футболки на рынке.
            Не забудь купить одну (wink)
            Ты можешь всегда заказать футболку онлайн или купить ее в нашем магазине.
            Адрес магазина ты можешь увидеть ниже.";

            var latitude = (float)52.362273;
            var longitude = (float)4.912178;

            await _botService.Client.SendTextMessageAsync(commandModel.ChatId, messageText);
            await _botService.Client.SendLocationAsync(commandModel.ChatId, latitude, longitude);
        }
    }
}
