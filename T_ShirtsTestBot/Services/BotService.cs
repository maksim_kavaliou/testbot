﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Telegram.Bot;

namespace T_ShirtsTestBot.Services
{
    public class BotService : IBotService
    {
        private string apiUrl = "https://api.telegram.org/bot";

        private readonly string _baseApiUrl;
        private readonly HttpClient _apiClient;
        private readonly BotConfiguration _botConfiguration;

        public BotService(IOptions<BotConfiguration> botConfiguration)
        {
            _botConfiguration = botConfiguration.Value;
            _baseApiUrl = $"{apiUrl}{_botConfiguration.Token}/";
            _apiClient = new HttpClient();
            Client = new TelegramBotClient(_botConfiguration.Token);
        }

        public async Task<HttpResponseMessage> GetAction(string apiAction)
        {
            var action = $"{_baseApiUrl}{apiAction}";
            var result = await _apiClient.GetAsync(action);

            return result;
        }

        public async Task<string> SetWebhook()
        {
            await Client.SetWebhookAsync(_botConfiguration.WebHookUrl);

            return await Task.FromResult(_botConfiguration.WebHookUrl);
        }

        public async Task DeleteWebhook()
        {
            await Client.DeleteWebhookAsync();
        }

        public TelegramBotClient Client { get; }
    }
}
