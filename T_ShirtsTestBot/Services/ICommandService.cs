﻿using System.Threading.Tasks;
using T_ShirtsTestBot.Commands.Models;

namespace T_ShirtsTestBot.Services
{
    public interface ICommandService
    {
        Task RunCommandAsync(CommandModel model);
    }
}
