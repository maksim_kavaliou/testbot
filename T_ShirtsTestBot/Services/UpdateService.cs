﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using T_ShirtsTestBot.Commands.Models;

namespace T_ShirtsTestBot.Services
{
    public class UpdateService : IUpdateService
    {
        private readonly ICommandService _commandService;
        private readonly Dictionary<string,Func<Update,CommandModel>> _commandsModelFactory = new Dictionary<string, Func<Update, CommandModel>>()
        {
            { "/start", (update) => new StartCommandModel(update) },
            { "/about", (update) => new AboutCommandModel(update) },
            { "/tshirtslist", (update) => new TshirtsListCommandModel(update) },
            { "/buy", (update) => new BuyCommandModel(update) },
            { "/prev_tshirt", (update) => new PreviousTshirtCommandModel(update) },
            { "/next_tshirt", (update) => new NextTshirtCommandModel(update) },

            // buttons
            { "о", (update) => new AboutCommandModel(update) },
            { "футболки", (update) => new TshirtsListCommandModel(update) },
        };

        public UpdateService(ICommandService commandService)
        {
            _commandService = commandService;
        }

        public async Task HandleUpdateAsync(Update update)
        {
            if (update.Type == UpdateType.Message)
            {
                if (update.Message.Type == MessageType.Text)
                {
                    var commandKey = update.Message.Text.Split(' ').First().ToLowerInvariant();
                    await RunCommand(commandKey, update);
                }
            }

            if (update.Type == UpdateType.CallbackQuery)
            {
                var commandKey = update.CallbackQuery.Data.Split(' ').First().ToLowerInvariant();
                await RunCommand(commandKey, update);
            }
        }

        public async Task RunCommand(string commandKey, Update update)
        {
            if (_commandsModelFactory.ContainsKey(commandKey))
            {
                await _commandService.RunCommandAsync(_commandsModelFactory[commandKey](update));
            }
            else
            {
                await _commandService.RunCommandAsync(new EchoCommandModel(update));
            }
        }
    }
}
