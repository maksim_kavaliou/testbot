﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace T_ShirtsTestBot.Services
{
    public interface IUpdateService
    {
        Task HandleUpdateAsync(Update update);
    }
}
