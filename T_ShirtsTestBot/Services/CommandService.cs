﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using T_ShirtsTestBot.Commands;
using T_ShirtsTestBot.Commands.Models;

namespace T_ShirtsTestBot.Services
{
    public class CommandService : ICommandService
    {
        private readonly Dictionary<CommandType, Func<CommandModel, Task>> _commands;

        public CommandService(IServiceProvider serviceProvider)
        {
            _commands = new Dictionary<CommandType, Func<CommandModel,Task>>
            {
                { CommandType.Start, (model) => serviceProvider.GetService<Command<StartCommandModel>>().Execute((StartCommandModel)model)},
                { CommandType.Echo, (model) => serviceProvider.GetService<Command<EchoCommandModel>>().Execute((EchoCommandModel)model) },
                { CommandType.About, (model) => serviceProvider.GetService<Command<AboutCommandModel>>().Execute((AboutCommandModel)model) },
                { CommandType.TshirtsList, (model) => serviceProvider.GetService<Command<TshirtsListCommandModel>>().Execute((TshirtsListCommandModel)model) },
                { CommandType.Buy, (model) => serviceProvider.GetService<Command<BuyCommandModel>>().Execute((BuyCommandModel)model) },
                { CommandType.NextTshirt, (model) => serviceProvider.GetService<Command<NextTshirtCommandModel>>().Execute((NextTshirtCommandModel)model) },
                { CommandType.PreviousTshirt, (model) => serviceProvider.GetService<Command<PreviousTshirtCommandModel>>().Execute((PreviousTshirtCommandModel)model) },
            };
        }

        public async Task RunCommandAsync(CommandModel model)
        {
            if (_commands.ContainsKey(model.CommandType))
            {
                await _commands[model.CommandType](model);
            }
            else
            {
                throw new NotImplementedException($"Command {model.CommandType} is not implemented");
            }
        }
    }
}
