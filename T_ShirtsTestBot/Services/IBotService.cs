﻿using System.Net.Http;
using System.Threading.Tasks;
using Telegram.Bot;

namespace T_ShirtsTestBot.Services
{
    public interface IBotService
    {
        TelegramBotClient Client { get; }

        Task<HttpResponseMessage> GetAction(string apiAction);

        Task<string> SetWebhook();

        Task DeleteWebhook();
    }
}
