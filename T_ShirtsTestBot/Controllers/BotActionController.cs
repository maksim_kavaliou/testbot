﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Controllers
{
    [Route("api/botaction")]
    public class BotActionController : Controller
    {
        private readonly IBotService _botService;

        public BotActionController(IBotService botService)
        {
            _botService = botService;
        }

        [HttpGet("getUpdates")]
        public async Task<ActionResult> GetUpdates()
        {
            var result = await _botService.GetAction("getUpdates");
            return Ok(result);
        }

        [HttpPost("setWebhook")]
        public async Task<ActionResult> SetWebhook()
        {
            var url = await _botService.SetWebhook();

            return Ok(new{webhookUrl = url});
        }

        [HttpPost("deleteWebhook")]
        public async Task<ActionResult> DeleteWebhook()
        {
            await _botService.DeleteWebhook();

            return Ok();
        }
    }
}
