﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using T_ShirtsTestBot.Services;

namespace T_ShirtsTestBot.Controllers
{
    [Route("api/update")]
    public class UpdateController : Controller
    {
        private readonly IUpdateService _updateService;

        public UpdateController(IUpdateService updateService)
        {
            _updateService = updateService;
        }

        // webhook
        // POST api/update
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Update update)
        {
            await _updateService.HandleUpdateAsync(update);
            return Ok();
        }
    }
}
