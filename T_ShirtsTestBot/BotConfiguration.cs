﻿namespace T_ShirtsTestBot
{
    public class BotConfiguration
    {
        public string Token { get; set; }

        public string WebHookUrl { get; set; }
    }
}
